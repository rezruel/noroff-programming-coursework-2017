import time

statement = []

names = []
completed = False

print("Today we're conducting a little survey, yes, yes, we get it, they're very annoying."
      " Please bear with us."
      " How many will be participating today?")


def interaction():
    number_of_users = input()

    try:
        users = int(number_of_users)
    except:
        print(f"Cannot convert {number_of_users} to integer.")
        return
    try:
        for num in range(users):
            print(f"Name of person #{num+1}?")
            name = input()
            print(f"Your favourite programming language is what, {name}?")
            language = input()
            statement.append('{} enjoys {} as a programming language.'.format(name, language))
            statement.append('\n')
            names.append(name)
    except:
        print(f"Invalid input, try a numerical input.")
        return

    print(''.join(statement))
    global completed
    completed = True


while not completed:
    interaction()
    print('Thank you for your time {}.'.format(', '.join(str(x) for x in names)))
    time.sleep(10)
